package receiver;

import java.util.HashMap;

import data.Variable;
import framework.MyIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmReceiver extends BroadcastReceiver{

	@Override
	 public void onReceive(Context context, Intent intent)
	{
		HashMap<String, String> ExtraMap = new HashMap<String, String>();
		ExtraMap.put("announce", "reminder. you have a "+Variable.reminder);
		MyIntent.StartService(context, Announcer.class, ExtraMap);
	}
	

}
