package receiver;

import java.util.HashMap;

import framework.MyIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class SmsReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();
		SmsMessage[] msgs = null;
		String number="";
		String msg="";
		if (bundle != null) {
			Object[] pdus = (Object[]) bundle.get("pdus");
			msgs = new SmsMessage[pdus.length];
			for (int i = 0; i < msgs.length; i++) {
				msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);	
				number = msgs[i].getOriginatingAddress();
				msg =msgs[i].getMessageBody().toString();
			}
			HashMap<String, String> ExtraMap = new HashMap<String, String>();
			ExtraMap.put("announce", "have a message from "+ContactReader.getName(context,number));
			ExtraMap.put("msg", msg);
			ExtraMap.put("num",number);
			MyIntent.StartService(context, Announcer.class, ExtraMap);
		}
	}
}