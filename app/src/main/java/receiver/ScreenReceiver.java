package receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import framework.Needs;

public class ScreenReceiver extends BroadcastReceiver {


	@Override
	public void onReceive(Context context, Intent intent) {

		if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
			Needs.Log("SCREEN_OFF");
		} else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
			Needs.Log("SCREEN_ON");
		}
	}
}