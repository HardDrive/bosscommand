package receiver;

import java.util.HashMap;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import framework.MyIntent;

public class BatteryReceiver extends BroadcastReceiver {
	private int level;
	public BatteryReceiver() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent i = context.registerReceiver(null, new IntentFilter(
				Intent.ACTION_BATTERY_CHANGED));
		level = i.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
		if (intent.getAction().equals(Intent.ACTION_BATTERY_LOW)) {
			//if (level==10) {
				HashMap<String, String> ExtraMap = new HashMap<String, String>();
				ExtraMap.put("announce", "battery low "+level+"%");
				MyIntent.StartService(context, Announcer.class, ExtraMap);
			//}
		} else if (intent.getAction().equals(Intent.ACTION_BATTERY_OKAY)) {
			HashMap<String, String> ExtraMap = new HashMap<String, String>();
			ExtraMap.put("announce", "my battery now is "+level+"%");
			MyIntent.StartService(context, Announcer.class, ExtraMap);
		} else if (intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED)) {
			HashMap<String, String> ExtraMap = new HashMap<String, String>();
			ExtraMap.put("announce", "battery full, please remove charger");
			MyIntent.StartService(context, Announcer.class, ExtraMap);
		} else if (intent.getAction().equals(Intent.ACTION_POWER_CONNECTED)) {
			HashMap<String, String> ExtraMap = new HashMap<String, String>();
			ExtraMap.put("announce","charger connect at "+level+"%");
			MyIntent.StartService(context, Announcer.class, ExtraMap);
		} else if (intent.getAction().equals(Intent.ACTION_POWER_DISCONNECTED)) {
			HashMap<String, String> ExtraMap = new HashMap<String, String>();
			ExtraMap.put("announce","charger remove at "+level+"%");
			MyIntent.StartService(context, Announcer.class, ExtraMap);
		}
	}
}
