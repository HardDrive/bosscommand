package receiver;

import java.util.HashMap;

import framework.MyIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;

public class CallReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle extras = intent.getExtras();
		if (extras != null) {
			String state = extras.getString(TelephonyManager.EXTRA_STATE);
			if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
				String number = extras
						.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
				HashMap<String, String> ExtraMap = new HashMap<String, String>();
				ExtraMap.put("announce", "have a call from "+ContactReader.getName(context, number));
				MyIntent.StartService(context, Announcer.class, ExtraMap);
			}
		}
	}
}