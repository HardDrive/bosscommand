package receiver;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

public class ContactReader {
	public static String getName(Context context, String number) {
		String name = "";
		String unknown = "";
		Uri uri;
		String[] projection;
		uri = Uri.withAppendedPath(
				ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
				Uri.encode(number));
		projection = new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME };
		Cursor cursor = context.getContentResolver().query(uri, projection,
				null, null, null);
		if (cursor != null) {
			if (cursor.moveToFirst()){
				name = cursor.getString(0);
				return name;
			}
			cursor.close();
		}
		for(char x: number.toCharArray()){
			unknown+=x+" ";
		}
		return unknown;
	}
}
