package receiver;

import java.util.HashMap;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;
import data.Variable;
import framework.MyIntent;

@SuppressWarnings("deprecation")
public class Announcer extends Service implements OnInitListener,
		OnUtteranceCompletedListener {
	private TextToSpeech mTts;
	private String announce;
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	public void onCreate() {
		mTts = new TextToSpeech(this, this);
	}

	@Override
	public void onDestroy() {
		mTts.shutdown();
	}
	
	@Override
	public void onStart(Intent intent, int startid) {
		Variable.number = intent.getStringExtra("num");
		Variable.message = intent.getStringExtra("msg");
		Variable.ifmessage = true;
		announce = intent.getStringExtra("announce");
		speech(announce);
	}

	@Override
	public void onUtteranceCompleted(String utteranceId) {
		if (utteranceId.equals("FINISHED PLAYING")) {
			if (isDoneSpeaking()) {
				shutdown();
			}
		}
	}

	public void onInit(int status) {
		if (status == TextToSpeech.SUCCESS) {
			mTts.setOnUtteranceCompletedListener(this);
			speech(announce);
		}
	}

	private void speech(String text) {
		AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		switch (am.getRingerMode()) {
		case AudioManager.RINGER_MODE_SILENT:
			shutdown();
			break;
		case AudioManager.RINGER_MODE_VIBRATE:
			shutdown();
			break;
		case AudioManager.RINGER_MODE_NORMAL:
			HashMap<String, String> myHashAlarm = new HashMap<String, String>();
			myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_STREAM,
					String.valueOf(AudioManager.STREAM_ALARM));
			myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID,
					"FINISHED PLAYING");
			mTts.speak(text, TextToSpeech.QUEUE_FLUSH, myHashAlarm);
			break;
		}
	}
	private boolean isDoneSpeaking() {
		if (!mTts.isSpeaking()) {
			return true;
		} else {
			return false;
		}
	}
	private void shutdown(){
		MyIntent.StopService(this, Announcer.class);
	}
}
