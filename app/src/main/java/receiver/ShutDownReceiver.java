package receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import data.Variable;

public class ShutDownReceiver extends BroadcastReceiver {
	
  @Override
	public void onReceive(Context context, Intent intent) {
	  Intent i =new Intent(context,Announcer.class);
	  if (intent.getAction().equals(Intent.ACTION_SHUTDOWN)) {
		  i.putExtra("name","Im shutting down. i gonna miss you "+Variable.user);
          context.startService(i);
		} 
	}

}