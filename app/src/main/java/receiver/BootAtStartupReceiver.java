package receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import framework.Needs;


public class BootAtStartupReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		Intent i =new Intent(context,Announcer.class);
		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			Needs.Log("ACTION_BOOT_COMPLETED");
			 i.putExtra("name","i'm here again running to your phoneO");
	          context.startService(i);
		}
	}
}
