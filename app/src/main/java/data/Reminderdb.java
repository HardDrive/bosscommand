package data;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import framework.Needs;

public class Reminderdb {
	private static final int VERSION = 1;
	private static final String DATA_BASE_NAME = "Reminderdb.db";
	private static final String TABLE_NAME = "userinfo";
	private static final String ID = "id";
	private static final String DATA1 = "firtname";
	private static final String DATA2 = "lastame";
	private static final String TABLE_CREATE = "create table " + TABLE_NAME
			+ " (" + ID +" INTEGER PRIMARY KEY AUTOINCREMENT," + DATA1 + " text not null," + DATA2
			+ " text not null)";

	private DataBaseHelper dbhelper;
	private Context context;
	private SQLiteDatabase db;

	public Reminderdb(Context context) {
		this.context = context;
		dbhelper = new DataBaseHelper(context);
	}

	private static class DataBaseHelper extends SQLiteOpenHelper {
		public DataBaseHelper(Context ctx) {
			super(ctx, DATA_BASE_NAME, null, VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			try {
				db.execSQL(TABLE_CREATE);
				Needs.Log("onCreate");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
			onCreate(db);
		}

	}
	public Reminderdb open() {
		db = dbhelper.getWritableDatabase();
		return this;
	}
	public void close() {
		dbhelper.close();
	}
	public void insertData(String data1, String data2) {
		open();
		ContentValues content = new ContentValues();
		content.put(DATA1, data1);
		content.put(DATA2, data2);
		db.insertOrThrow(TABLE_NAME, null, content);
		close();
	}
	public boolean deleteAllData() {
		return context.deleteDatabase(DATA_BASE_NAME);
	}
	public void update(Integer id, String data1) {
		open();
		ContentValues values = new ContentValues();
		values.put(DATA1, data1);
		db.update(TABLE_NAME, values, "id=?", new String[] { id.toString() });
		close();
	}
	public void update(String olddata1, String newdata1) {
		open();
		ContentValues values = new ContentValues();
		values.put(DATA1, newdata1);
		db.update(TABLE_NAME, values, DATA1+"=?", new String[] { olddata1 });
		close();
	}
	public int delete(String id){
		open();
		String whereArgs[]={id};
		int count = db.delete(TABLE_NAME, ID+" =? ", whereArgs);
		return count;
	}
	public ArrayList<String> SearchList(String data1){
		ArrayList<String> list = new ArrayList<String>();
		open();
		String columns[]={ID,DATA1,DATA2};
		Cursor cursor =  db.query(TABLE_NAME, columns, DATA1+" =? " +data1, null, null, null, null);
		while (cursor.moveToNext()) {
			list.add(cursor.getInt(0)+" "+cursor.getString(1)+" "+cursor.getString(2));
		}
		close();
		return list;
	}
	public ArrayList<String> getAllList(){
		ArrayList<String> list = new ArrayList<String>();
		open();
		String columns[]={ID,DATA1,DATA2};
		Cursor cursor =  db.query(TABLE_NAME, columns, null, null, null, null, null);
		while (cursor.moveToNext()) {
			list.add(cursor.getString(2));
		}
		close();
		return list;
	}
}
