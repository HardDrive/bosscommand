package data;

import framework.Needs;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Appdata {
	private static final int VERSION = 1;
	private static final String DATA_BASE_NAME = "Appdb.db";
	private static final String TABLE_NAME = "userinfo";
	private static final String ID = "id";
	private static final String DATA1 = "appname";
	private static final String DATA2 = "package";
	private static final String TABLE_CREATE = "create table " + TABLE_NAME
			+ " (" + ID + "," + DATA1 + " text not null," + DATA2
			+ " text not null)";

	private DataBaseHelper dbhelper;
	private Context context;
	private SQLiteDatabase db;

	public Appdata(Context context) {
		this.context = context;
		dbhelper = new DataBaseHelper(context);
	}

	private static class DataBaseHelper extends SQLiteOpenHelper {
		public DataBaseHelper(Context ctx) {
			super(ctx, DATA_BASE_NAME, null, VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			try {
				db.execSQL(TABLE_CREATE);
				Needs.Log("onCreate");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
			onCreate(db);
		}

	}

	public Appdata open() {
		db = dbhelper.getWritableDatabase();
		return this;
	}

	public void close() {
		dbhelper.close();
	}

	public long insertData(String data1, String data2) {
		ContentValues content = new ContentValues();
		content.put(DATA1, data1);
		content.put(DATA2, data2);
		return db.insertOrThrow(TABLE_NAME, null, content);
	}

	public Cursor SelectAll() {
		return db.query(TABLE_NAME, new String[] { DATA1, DATA2 }, null, null,
				null, null, null);
	}

	public Cursor Where(String find) {
		return db.query(TABLE_NAME, null, DATA1 + "=?", new String[] { find },
				null, null, null);
	}

	public boolean deleteAllData() {
		return context.deleteDatabase(DATA_BASE_NAME);
	}

	public void deleteWhere(String find) {
		open();
		db.delete(TABLE_NAME, DATA1 + "=?", new String[] { find });
		open();
	}
	public void updateData(Integer id, String data1) {
		open();
		ContentValues values = new ContentValues();
		values.put(DATA1, data1);
		db.update(TABLE_NAME, values, "id=?", new String[] { id.toString() });
		close();
	}
	public void updateData(String olddata1, String newdata1) {
		open();
		ContentValues values = new ContentValues();
		values.put(DATA1, newdata1);
		db.update(TABLE_NAME, values, DATA1+"=?", new String[] { olddata1 });
		close();
	}
	public int getcount() {
		int count = 0;
		open();
		Cursor C = db.query(TABLE_NAME, new String[] { DATA1, DATA2 }, null, null,
				null, null, null);
		if (C.moveToFirst()) {
			do {
				count++;
			} while (C.moveToNext());
		}
		close();
		return count;
	}
}
