package framework;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import static android.content.Context.CAMERA_SERVICE;

public class FlashLightHandler {

	Context context;
	CameraManager cameraManager;
	String cameraID ;

	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
	public FlashLightHandler(Context context) {
		this.context = context;
		cameraManager = (CameraManager) context.getSystemService(CAMERA_SERVICE);
		try {
			cameraID = cameraManager.getCameraIdList()[0];
		} catch (CameraAccessException e) {
			e.printStackTrace();
		}
	}

	@TargetApi(Build.VERSION_CODES.M)
	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
	public void setOn() {
		try {
			cameraManager.setTorchMode(cameraID, true);
		} catch (CameraAccessException e) {
			e.printStackTrace();
		}
	}

	@TargetApi(Build.VERSION_CODES.M)
	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
	public void setOff() {
		try {
			cameraManager.setTorchMode(cameraID,false);
		} catch (CameraAccessException e) {
			e.printStackTrace();
		}
	}

	public void toggle() {
	}
}
