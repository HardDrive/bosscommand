package framework;

import com.example.voicecommand.VoiceEngine;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;

public class SmsSenderHandler {
	private  String ACTION_SMS_SENT = "com.example.smsreceiver.SMS_SENT_ACTION";
	private  Context context;
	public SmsSenderHandler(Context contex){
		this.context = contex;
		 contex.registerReceiver(new BroadcastReceiver() {
	            @Override
	            public void onReceive(Context context, Intent intent) {
	                switch (getResultCode()) {
	                case Activity.RESULT_OK:
	                	 Needs.Log("Send");
	                	 VoiceEngine.speech("Message is send");
	                    break;
	                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
	                	 Needs.Log("Error");
	                	 VoiceEngine.speech("Error");
	                    break;
	                case SmsManager.RESULT_ERROR_NO_SERVICE:
	                	 Needs.Log("no service");
	                	 VoiceEngine.speech("no service");
	                    break;
	                case SmsManager.RESULT_ERROR_NULL_PDU:
	                	Needs.Log("NULL_PDU");
	                    break;
	                case SmsManager.RESULT_ERROR_RADIO_OFF:
	                	Needs.Log("RADIO_OFF");
	                    break;
	                }
	            }
	        }, new IntentFilter(ACTION_SMS_SENT));
	}
	public void send(String msg,String num){
		  SmsManager sms = SmsManager.getDefault();
          sms.sendTextMessage(num, null, msg, PendingIntent.getBroadcast(
                  context, 0, new Intent(ACTION_SMS_SENT), 0), null);
	}
	
}
