package framework;

import java.util.Calendar;

public class CalendarHandler {
	private static Calendar c = Calendar.getInstance();
	public static int getHour(){
		return c.get(Calendar.HOUR_OF_DAY);
	}
	public static int getMinute(){
		return c.get(Calendar.MINUTE);
	}
	public static int getAM_PM(){
		return c.get(Calendar.AM_PM);
	}
	public static String getAM_PMstr(){
		if(c.get(Calendar.AM_PM)==0){
			return "AM";
		}else {
			return "PM";
		}
	}
	public static int getDate(){
		return c.get(Calendar.DATE);
	}
	public static int getDay(){
		return c.get(Calendar.DAY_OF_WEEK);
	}
	public static String getDaystr(){
		switch (c.get(Calendar.DAY_OF_WEEK)) {
		case 1:
			return "Sunday";
		case 2:
			return "Monday";
		case 3:
			return "Tuesday";
		case 4:
			return "Wednesday";
		case 5:
			return "Thursday";
		case 6:
			return "Friday";
		case 7:
			return "Saturday";
		default:
			return "null";
		}
	}
	public static int getMonth(){
		return c.get(Calendar.MONTH);
	}
	public static String getMonthstr(){
		switch (c.get(Calendar.MONTH)) {
		case 0:
			return "January";
		case 1:
			return "February";
		case 2:
			return "March";
		case 3:
			return "April";
		case 4:
			return "May";
		case 5:
			return "June";
		case 6:
			return "July";
		case 7:
			return "August";
		case 8:
			return "September";
		case 9:
			return "October";
		case 10:
			return "November";
		case 11:
			return "December";
		default:
			return "unDecember";
		}
	}
	public static int getYear(){
		return c.get(Calendar.YEAR);
	}
	public static Calendar GetCalendar(){
		return c;
	}
	public static void setHour(int hour){
		c.set(Calendar.HOUR_OF_DAY, hour);
	}
	public static void setMinute(int minute){
		c.set(Calendar.MINUTE, minute);
	}
	public static void setDate(int date){
		c.set(Calendar.DAY_OF_MONTH, date);
	}
	public static void setMonth(int month){
		c.set(Calendar.MONTH, month);
	}
	public static void setYear(int year){
		c.set(Calendar.MONTH, year);
	}
}
