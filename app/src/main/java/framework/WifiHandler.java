package framework;

import android.content.Context;
import android.net.wifi.WifiManager;

public class WifiHandler {
	private WifiManager wifi;
	private boolean isOn = false;
	public WifiHandler(Context context) {
		wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
	}

	public void on() {
		wifi.setWifiEnabled(true);
		isOn = true;
	}

	public void off() {
		wifi.setWifiEnabled(false);
		isOn = false;
	}
	public void toggle(){
		if(isOn){
			off();
		}else {
			on();
		}
	}
}
