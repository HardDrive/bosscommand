package framework;

import java.io.IOException;

import android.media.MediaRecorder;

public class MicrophoneHandler {
	MediaRecorder recorder;
	int level;
	
	public MicrophoneHandler(){
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile("/dev/null");
       try {
			recorder.prepare();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        recorder.start();	
	}
	public int getLevel(){
		level = recorder.getMaxAmplitude();
		level = (level/2000)%20;
		return level;
	}
	public int getAmplitude(){
		if (recorder != null)
			return recorder.getMaxAmplitude();
		else
			return 0;
	}
}
