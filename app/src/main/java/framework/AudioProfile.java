package framework;

import android.content.Context;
import android.media.AudioManager;

public class AudioProfile {
	private AudioManager am;
	
	public AudioProfile(Context context) {
		am = (AudioManager) context.getSystemService(context.AUDIO_SERVICE);
	}
	public void normal() {
		am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
	}
	public void silent() {
		am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
	}
	public void vibrate() {
		am.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
	}

}
