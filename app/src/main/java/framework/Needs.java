package framework;
/*
 * author John Carlo Franco from ProgDev inc
 */
import android.content.Context;
import android.provider.SyncStateContract.Constants;
import android.util.Log;
import android.widget.Toast;
public class Needs {
	
	public static void Toast(Context context, String message){
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}
	public static void Log(String log){
		Log.e(Constants._ID, log);
	}
}
