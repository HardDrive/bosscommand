package framework;
/*
 * author John Carlo Franco from ProgDev inc
 */
import android.content.Context;
import android.os.Vibrator;

public class VibrateHandler {
	private Vibrator vibrator;
	long[] pattern = { 0,200, 200, 200, 200, 200,500, 500, 200, 500, 200, 1000,
			500, 200, 200, 200, 200, 200,500};
	public VibrateHandler(Context context){
		vibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
	}
	public void Vibrate(){
		vibrator.vibrate(300);
	}
	public void milliseconds(long milliseconds){
		vibrator.vibrate(milliseconds);
	}
	public void Pattern(){
		vibrator.vibrate(pattern, -1);
	}
	public void PatternAdd(int one,int two,int three){
		long[] patterns = {0,one, one, one, one, one,two, two, one, two, one, two,
				two, one, one, one, one, one,three};
		vibrator.vibrate(patterns, -1);
	}
	public void ON(){
		vibrator.vibrate(new long[] { 0, 200, 0 }, 0);
	}
	public void OFF(){
		vibrator.cancel();
	}
}
