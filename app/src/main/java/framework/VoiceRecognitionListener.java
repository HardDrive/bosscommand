package framework;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;

import com.example.voicecommand.VoiceEngine;

public class VoiceRecognitionListener implements RecognitionListener {
    public String TAG = VoiceRecognitionListener.class.getSimpleName();
    private SpeechRecognizer sr;
    private VoiceEngine eng;

    public VoiceRecognitionListener(Context context, Activity activty) {
        sr = SpeechRecognizer.createSpeechRecognizer(context);
        sr.setRecognitionListener(this);
        eng = new VoiceEngine(context, activty);
    }

    public void onResults(Bundle data) {
        Log.e(TAG,"onResults");
        ArrayList<String> matches = data
                .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        float[] value = data.getFloatArray(SpeechRecognizer.CONFIDENCE_SCORES);
        if (value != null) {
            for (int i = 0; i < matches.size(); i++) {
                Needs.Log(matches.get(i).toString());
                if (eng.gotword(matches.get(i).toString())) {
                    break;
                }
            }
        }
    }

    public void listen() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PREFER_OFFLINE, true);
        sr.startListening(intent);
    }

    public void onBeginningOfSpeech() {
        // when detect sound
        eng.onBeginningOfSpeech();
        Log.e(TAG,"onBeginningOfSpeech");
    }

    public void onBufferReceived(byte[] buffer) {
        Log.e(TAG,"onBufferReceived");
    }

    public void onEndOfSpeech() {
        // Waiting for result...
        eng.onEndOfSpeech();
        Log.e(TAG,"onEndOfSpeech");
    }

    public void onError(int error) {
        // error result
//        eng.onError();
        Log.e(TAG,"onError");
    }

    public void onEvent(int eventType, Bundle params) {
        Log.e(TAG,"onEvent");
    }

    public void onPartialResults(Bundle partialResults) {
        Log.e(TAG,"onPartialResults");
    }

    public void onReadyForSpeech(Bundle params) {
        // ready to get command
        eng.onReadyForSpeech();
        Log.e(TAG,"onReadyForSpeech");
    }

    public void onRmsChanged(float rmsdB) {
        // when listening
        eng.onRmsChanged();
        Log.e(TAG,"onRmsChanged");
    }
}