package framework;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class Regex {
	
	
	public static boolean Contains(String str,String contain){
	    return str.matches(".*"+contain+".*");
	}
	public static String Replace(String str,String old,String newstr){
		return str.replaceAll(old, "\\"+newstr);
	}
	public static boolean Matches(String str,String pattern){
	    return str.matches(pattern);
	}
	public static String Filter(String str,String pattern){
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(str);
		if (m.find()) {
			return m.group(1);
		}
		return null;
	}

}
