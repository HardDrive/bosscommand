package framework;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map.Entry;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Toast;

public class MyIntent {
	public static void StartNewActivity(Context context, Class<?> cls) {
		Intent i = new Intent(context, cls);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		context.startActivity(i);
	}

	public static void StartActivity(Context context, Class<?> cls) {
		Intent i = new Intent(context, cls);
		context.startActivity(i);
	}

	public static void StartActivity(Context context, Class<?> cls, HashMap<String, String> ExtraMap) {
		Intent i = new Intent(context, cls);
		for (Entry<String, String> x : ExtraMap.entrySet()) {
			i.putExtra(x.getKey(), x.getValue());
		}
		context.startActivity(i);
	}

	public static void StartService(Context context, Class<?> cls) {
		context.startService(new Intent(context, cls));
	}

	public static void StartService(Context context, Class<?> cls, HashMap<String, String> ExtraMap) {
		Intent i = new Intent(context, cls);
		for (Entry<String, String> x : ExtraMap.entrySet()) {
			i.putExtra(x.getKey(), x.getValue());
		}
		context.startService(i);
	}

	public static void StopService(Context context, Class<?> cls) {
		context.stopService(new Intent(context, cls));
	}

	public static void ShareLink(Activity activity, String link) {
		Intent share = new Intent(android.content.Intent.ACTION_SEND);
		share.setType("text/plain");
		share.putExtra(Intent.EXTRA_TEXT, link);
		activity.startActivity(Intent.createChooser(share, "Share!"));

	}

	public static void ShareImage(Activity activity, String locationfile) {
		Intent share = new Intent(Intent.ACTION_SEND);
		share.setType("image/*");
		File file = new File(Environment.getExternalStorageDirectory(),
				locationfile + ".jpg");
		Uri uri = Uri.fromFile(file);
		share.putExtra(Intent.EXTRA_STREAM, uri);
		activity.startActivity(Intent.createChooser(share, "Share!"));
	}

	public static void Visitlink(Activity activity, String url) {
		Uri uri = Uri.parse("http://" + url);
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		activity.startActivity(intent);
	}

	public static void OpenSystemApps(Activity activity, String packagename) {
		Intent intent = new Intent();
		try {
			intent.setAction(packagename);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			activity.startActivity(intent);
		} catch (Exception e) {
		}
	}

	public static void OpenApps(Activity activity, String packagename) {
		Intent i;
		PackageManager manager = activity.getPackageManager();
		try {
			i = manager.getLaunchIntentForPackage(packagename);
			if (i == null)
				throw new PackageManager.NameNotFoundException();
			i.addCategory(Intent.CATEGORY_LAUNCHER);
			activity.startActivity(i);
		} catch (PackageManager.NameNotFoundException e) {
		}
	}

	public static void takeScreenshot(Activity activity, int id, String locationfile) {
		View v = activity.findViewById(id);
		v.setDrawingCacheEnabled(true);
		Bitmap bitmap = v.getDrawingCache();
		File file = new File(Environment.getExternalStorageDirectory(),
				locationfile + ".jpg");
		try {
			FileOutputStream stream = new FileOutputStream(file);
			bitmap.compress(CompressFormat.JPEG, 100, stream);
			stream.flush();
			stream.close();
		} catch (IOException e) {
		} finally {
			v.setDrawingCacheEnabled(false);
		}

	}

	public static void GotoHome(Activity activity) {
		Intent startMain = new Intent(Intent.ACTION_MAIN);
		startMain.addCategory(Intent.CATEGORY_HOME);
		startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		activity.startActivity(startMain);
	}

	public static void Alarm(Context context, Class<?> ReceiverCls, Calendar calendar) {
		Intent i = new Intent(context, ReceiverCls);
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_ONE_SHOT);
		@SuppressWarnings("static-access")
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
		alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pi);
	}

	public static void goToDial(Activity activity) {
        Uri number = Uri.parse("tel:");
        Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
        activity.startActivity(callIntent);
	}
}
