package framework;

import android.os.Handler;

import com.example.voicecommand.FloatingMic;
import com.example.voicecommand.MainActivity;

public class ThreadHandler {
	private Handler myHandler;
	private int timer;
	public ThreadHandler(int timer) {
			this.timer =timer;
	}
	private Runnable myRunnable = new Runnable() {
		@Override
		public void run() {
			myHandler.postDelayed(this, timer);
			FloatingMic.runningtheread();
		}
	};
	public void start(){
		myHandler = new Handler();
		myHandler.post(myRunnable);
	}
	public void stop(){
		 myHandler.removeCallbacks(myRunnable); 
	}
}
