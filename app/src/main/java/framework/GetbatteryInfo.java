package framework;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

public class GetbatteryInfo {
	private static int level;
	private boolean plugged;
	private String technogy;
	private static int temperature;
	public GetbatteryInfo(Context context){
		context.registerReceiver(mBatInfoReceiver, new IntentFilter(
				Intent.ACTION_BATTERY_CHANGED));
	}
	private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context c, Intent intent) {
			level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
			if(intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0)>0){
				plugged = true;
			}else {plugged = false;}
			technogy = intent.getExtras().getString(BatteryManager.EXTRA_TECHNOLOGY);
			temperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,0);
		}
	};
	public static int Getlevel(){
		return level;
	}
	public boolean Getplugged(){
		return plugged;
	}
	public String Gettechnogy(){
		return technogy;
	}
	public static int Gettemperature(){
		return temperature;
	}
}
