package framework;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;

public class BluetoothHandler {
	private BluetoothAdapter mAdapter;
	private Activity activity;
	private boolean isOn = false;
	public BluetoothHandler(Activity activity){
		this.activity = activity;
		mAdapter = BluetoothAdapter.getDefaultAdapter();
	}
	public void on(){
		if (mAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
			Intent discoverableIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			discoverableIntent.putExtra(
					BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
			activity.startActivity(discoverableIntent);
			isOn = true;
		}
	}
	public void off(){
		if (mAdapter.isEnabled()) {
			mAdapter.disable(); 
			isOn = false;
		} 
	}
	public void toggle(){
		if(isOn){
			on();
		}else {
			off();
		}
	}
}
