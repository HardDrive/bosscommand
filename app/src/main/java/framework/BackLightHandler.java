package framework;

import android.app.Activity;
import android.content.Context;
import android.view.WindowManager;

public class BackLightHandler {
	float BackLightValue = 0f;
	private Context context;
	private Activity activty;
	private boolean isUp = false;

	public BackLightHandler(Context context,Activity activty) {
		this.context = context;
		this.activty = activty;

	}

	public void set() {
		int SysBackLightValue = (int) (BackLightValue * 255);
		android.provider.Settings.System.putInt(context.getContentResolver(),
				android.provider.Settings.System.SCREEN_BRIGHTNESS,
				SysBackLightValue);
	}
	public void adjustUp(){
		BackLightValue = .9f;
		setlight(BackLightValue);
		isUp = true;
	}
	public void adjustDown(){
		BackLightValue = .0f;
		setlight(BackLightValue);
		isUp = false;
	}
	private void setlight(float value){
		WindowManager.LayoutParams layoutParams = activty.getWindow().getAttributes();
		   layoutParams.screenBrightness = value;
		   activty.getWindow().setAttributes(layoutParams);
	}
	public void toggle(){
		if(isUp){
			adjustDown();
		}else {
			adjustUp();
		}
	}
}
