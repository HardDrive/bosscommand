package framework;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.method.PasswordTransformationMethod;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.voicecommand.MainActivity;
import com.example.voicecommand.R;

import data.Variable;

public class Dialogs {
	private Context context;
	public Dialogs(Context context) {
		this.context = context;
	}

	public void Radiobutton() {
		final CharSequence[] items = { "John", "Rey", "Villaganas", "Franco" };
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Alert Dialog with ListView and Radio button");
		builder.setIcon(R.mipmap.ic_launcher);
		builder.setSingleChoiceItems(items, -1,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {
						Toast.makeText(context, items[item], Toast.LENGTH_SHORT)
								.show();
						dialog.cancel();
					}
				});
		builder.show();
	}

	public void List() {
		final CharSequence[] items = { "John", "Rey", "Villaganas", "Franco" };
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Alert Dialog with ListView");
		builder.setIcon(R.mipmap.ic_launcher);
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				Toast.makeText(context, items[item], Toast.LENGTH_SHORT).show();
			}
		});
		builder.show();
	}

	public void Button() {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("EXIT...");
		builder.setIcon(android.R.drawable.ic_menu_info_details);
		builder.setMessage("Do you want to exit ??");
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				Toast.makeText(context, "youclick ok", Toast.LENGTH_SHORT)
						.show();
			}
		});
		builder.setNegativeButton("No", null);
		builder.show();

	}

	public void message(String title,String msg) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setIcon(android.R.drawable.ic_menu_info_details);
		builder.setTitle(title);
		builder.setMessage(msg);
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				
			}
		});
		builder.show();
	}

	public void input() {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		LinearLayout mydialog = new LinearLayout(context);
		mydialog.setOrientation(LinearLayout.VERTICAL);
		final EditText input = new EditText(context);
		final EditText inputtwo = new EditText(context);
		final EditText inputthree = new EditText(context);
		builder.setTitle("What i call to you?");
		mydialog.addView(input);
		mydialog.addView(inputtwo);
		mydialog.addView(inputthree);
		input.setHint("Enter your Username");
		inputtwo.setHint("Enter your password");
		inputtwo.setTransformationMethod(PasswordTransformationMethod.getInstance());
		inputthree.setHint("Repeat password");
		inputthree.setTransformationMethod(PasswordTransformationMethod.getInstance());
		builder.setView(mydialog);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String in2 = inputtwo.getText().toString();
				String in3 = inputthree.getText().toString();
				if (in2.contentEquals(in3)) {
					Variable.user = input.getText().toString();
					Intent i = new Intent(context, MainActivity.class);
					context.startActivity(i);
				}else {
					Needs.Toast(context,"Password Not match");
					 input();
				}

			}
		});
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		builder.show();
	}
}
