package floating;

import android.app.Notification;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.view.HapticFeedbackConstants;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;

public abstract class FloatingService extends Service implements OnTouchListener {
	
	private MovableFloatingView view;
	private static int notificationId = 2;
	private static Notification notification;
	private WindowManager wm;
	private BroadcastReceiver receiver;
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	@Override
	public void onCreate() {
		super.onCreate();
		this.wm = (WindowManager) this.getSystemService(WINDOW_SERVICE);
		View v = this.getFloatingView();
		this.view = new MovableFloatingView(v, this.getClass().getCanonicalName());
		this.view.setOnTouchListener(this);
		Point p = this.getInitPosition();
		this.view.init(this.wm, p.x, p.y);
		if (notification == null) {
			NotificationCompat.Builder builder = new NotificationCompat.Builder(this.getApplicationContext());
			notification = builder.build();
		}
		IntentFilter filter = new IntentFilter();
		filter.addAction(OrientationChangedReceiver.BCAST_CONFIGCHANGED);
		if (this.receiver == null) {
			receiver = new OrientationChangedReceiver(this.view);
		}
		this.registerReceiver(receiver, filter);
	}
	protected abstract View getFloatingView();
	protected Point getInitPosition() {
		Point p = new Point(0, 0);
		return p;
	}
	public boolean isMoving() {
		return this.view.isMoving();
	}
	public boolean isMoved() {
		return this.view.isMoved();
	}
	public boolean isTouching() {
		return this.view.isTouching();
	}
	public boolean isForceStable() {
		return this.view.isForceStable();
	}
	public void setForceStable(boolean stable) {
		this.view.setForceStable(stable);
	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		this.view.updateViewPosition();
		this.startForeground(notificationId, notification);
		return START_STICKY;
	}
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (this.receiver != null) {
			this.unregisterReceiver(receiver);
		}
		this.view.removeView();
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			if (v != null) {
				v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
			}
		}
		return false;
	}
	protected WindowManager getWm() {
		return wm;
	}
	protected Point getAdjustedViewPosition(Point p) {
	    return this.view.getAdjustedViewPosition(p);
	}
}
