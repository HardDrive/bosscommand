package floating;

public interface ConstantsFloating {
	String GLOBAL_KEY = "Global.Key";
	String ENABLED = "/enabled";
	String X = "x";
	String Y = "y";
	String OR = "isPortrait";
	String SELF_START = "SelfStart";
	int[] angles = { 0, 90, 180, 270 };
	int[][] offsets = { { -1, 0 }, { 0, -1 }, { 1, 0 }, { 0, 1 } };
	int[][][][] xoffsets = {
			{ { { 1, 0, -1 }, { 0, -1, -1 } }, { { 0, -1, -1 }, { 1, 0, -1 } } },
			{ {}, { { 1, 1, 0 }, { 1, 0, -1 } }, { { 0, 0, 0 }, { 0, -1, -1 } } },
			{ {}, {}, { { 0, 0, 0 }, { 1, 1, 0 } },
					{ { 1, 1, 0 }, { 0, 0, 0 } } },
			{ { { 1, 0, -1 }, { 1, 1, 0 } }, {}, {},
					{ { 0, -1, -1 }, { 0, 0, 0 } } } };
}
