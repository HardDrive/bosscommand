package floating;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import floating.OrientationChangedReceiver.IOrientationChangedListener;


public class MovableFloatingView implements IOrientationChangedListener {
	private View view;
	private SharedPreferences sp;
	private final static int THRESHOLD = 20;
	
	private OnTouchListener onTouchListener;
	
	private WindowManager wm;
	private LayoutParams lp;
	private boolean added;
	
	private Rect r = new Rect();
	
	private int orientation;
	
	private boolean moving;
	private boolean moved;
	private boolean touching;
	private boolean forceStable;
	
	private OnTouchListener touchMove = new OnTouchListener() {
		private float x;
		private float y;
	    private Point p = new Point();
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			int action = event.getAction();
			switch(action) {
			case MotionEvent.ACTION_DOWN:
			    p = getAdjustedViewPosition(p);
				this.x = event.getRawX() - p.x;
				this.y = event.getRawY() - p.y;
				setTouching(true);
				setMoved(false);
				break;
			case MotionEvent.ACTION_MOVE:
			    p = getAdjustedViewPosition(p);
				int nx = (int)(event.getRawX() - x);
				int ny = (int)(event.getRawY() - y);
				int dx = Math.abs(nx - p.x);
				int dy = Math.abs(ny - p.y);
				if (!isForceStable() && (isMoving() || dx > THRESHOLD || dy > THRESHOLD)) {
					MovableFloatingView.this.calcViewPostion((int)(nx), (int)(ny));
					MovableFloatingView.this.updateViewPosition();
					setMoving(true);
					setMoved(true);
				}
				break;
			case MotionEvent.ACTION_UP:
				setMoving(false);
				setTouching(false);
				save();
				break;
			}
			
			if (onTouchListener != null) {
				onTouchListener.onTouch(v, event);
			}
			return false;
		}
	};
	
	private void save() {
		SharedPreferences.Editor editor = sp.edit();
		editor.putInt(ConstantsFloating.X, lp.x);
		editor.putInt(ConstantsFloating.Y, lp.y);
		editor.putInt(ConstantsFloating.OR, this.orientation);
		editor.commit();
	}
	public MovableFloatingView(View view, String key) {
		this.view = view;
		this.sp = view.getContext().getSharedPreferences(key, Context.MODE_PRIVATE);
	}
	public Point getAdjustedViewPosition(Point p) {
	    int x = lp.x;
	    int y = lp.y;
	    this.view.getRootView().getWindowVisibleDisplayFrame(r);
	    if (x < 0) {
	        x = 0;
	    } else if (x + this.view.getWidth() > r.width()) {
	        x = r.width() - this.view.getWidth();
	    }
	    if (y < 0) {
	        y = 0;
	    } else if (y + this.view.getHeight() > r.height()) {
	        y = r.height() - this.view.getHeight();
	    }
	    if (p == null) {
	        p = new Point(x, y);
	    } else {
	        p.x = x;
	        p.y = y;
	    }
	    return p;
	}
	public void updateViewPosition() {
		if (this.added) {
			this.wm.updateViewLayout(this.view, this.lp);
		} else {
			this.wm.addView(this.view, this.lp);
			this.added = true;
		}
	}
	public void removeView() {
		if (this.added) {
			this.wm.removeView(this.view);
			this.added = false;
		}
	}
	protected void calcViewPostion(int x, int y) {
		this.lp.x = x;
		this.lp.y = y;
	}
	public void init(WindowManager wm, int x, int y) {
		this.wm = wm;
		this.lp = new LayoutParams(
				LayoutParams.WRAP_CONTENT, 
				LayoutParams.WRAP_CONTENT, 
				LayoutParams.TYPE_SYSTEM_ERROR,
				LayoutParams.FLAG_NOT_FOCUSABLE, 
				PixelFormat.TRANSPARENT);
		this.lp.gravity = Gravity.TOP | Gravity.LEFT;
		this.lp.x = this.sp.getInt(ConstantsFloating.X, x);
		this.lp.y = this.sp.getInt(ConstantsFloating.Y, y);
		this.orientation = this.sp.getInt(ConstantsFloating.OR, Configuration.ORIENTATION_PORTRAIT);
		this.onOrientationChanged( this.view.getResources().getConfiguration().orientation);
		this.view.setOnTouchListener(this.touchMove);
	}
	public void init(WindowManager wm) {
		this.init(wm, 0, 0);
	}
	public boolean isMoving() {
		return moving;
	}
	public void setMoving(boolean moving) {
		this.moving = moving;
	}
	public boolean isTouching() {
		return touching;
	}
	public void setTouching(boolean touching) {
		this.touching = touching;
	}
	public boolean isForceStable() {
		return forceStable;
	}
	public void setForceStable(boolean forceStable) {
		this.forceStable = forceStable;
		if (forceStable) {
			this.setMoving(false);
		}
	}
	public void setOnTouchListener(OnTouchListener onTouchListener) {
		this.onTouchListener = onTouchListener;
	}
	public boolean isMoved() {
		return moved;
	}
	public void setMoved(boolean moved) {
		this.moved = moved;
	}
	@Override
	public void onOrientationChanged(int orientation) {
		if (this.orientation != orientation) {
			View root = this.view.getRootView();
			root.getWindowVisibleDisplayFrame(r);
			int x = lp.x;
			int y = lp.y;
			int vw = this.view.getWidth();
			int vh = this.view.getHeight();
			int w = r.width();
			int h = r.height();
			int ow = h + r.top;
			int oh = w - r.top;
			lp.x = (x < (ow - vw) >> 1) ? x * w / ow : (x * w + (w - ow) * vw) / ow;
			lp.y = (y < (oh - vh) >> 1) ? y * h / oh : (y * h + (h - oh) * vh) / oh;
			this.orientation = orientation;
			this.updateViewPosition();
			this.onTouchListener.onTouch(this.view, MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_CANCEL, 0, 0, 0));
		}
	}
}
