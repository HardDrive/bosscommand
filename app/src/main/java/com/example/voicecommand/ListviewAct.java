package com.example.voicecommand;

import android.R.string;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import data.Appdata;


public class ListviewAct extends ListActivity {

	private static Appdata Sql;
	private String[] mylist;
    @Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      Sql = new Appdata(getApplicationContext());
      display();
    }
    protected void onListItemClick(ListView l, View v, int position, long id) {
     super.onListItemClick(l, v, position, id);
     dialog(position);
    }
    public void dialog(final int position){
    	AlertDialog.Builder builder = new AlertDialog.Builder(this,AlertDialog.THEME_DEVICE_DEFAULT_DARK);
		builder.setTitle("Action");
		builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() { 
		    @Override
		    public void onClick(DialogInterface dialog, int which) {		  				    		 
		    	Sql.deleteWhere(mylist[position]);
		        display();
		    }
		});	
		builder.setNegativeButton("Edit", new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		    	edit(mylist[position]);
		    }
		});
		builder.show();
    }
    public void edit(final String app){
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final EditText input = new EditText(this);
		input.setText(app);
		builder.setTitle("Edit");
		builder.setView(input);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
		    @Override
		    public void onClick(DialogInterface dialog, int which) {		  				    		 
		    	Sql.updateData(app, input.getText().toString());
		    	display();
		    }
		});	
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        dialog.cancel();
		    }
		});
		builder.show();

    }
    private void display(){
    	mylist = new String[Sql.getcount()];
        Sql.open();
  		Cursor C = Sql.SelectAll();
  		int i = 0;
  		if(C.moveToFirst()){
  			do {
  				mylist[i]=C.getString(0);
  				i++;
  			}while(C.moveToNext());
  		}
  		Sql.close();
       this.setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mylist));
    }
}
