package com.example.voicecommand;

import receiver.MyAdminReceiver;
import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;

import data.Variable;
import framework.AudioProfile;
import framework.BackLightHandler;
import framework.BluetoothHandler;
import framework.CalendarHandler;
import framework.FlashLightHandler;
import framework.GetbatteryInfo;
import framework.MyIntent;
import framework.Needs;
import framework.Regex;
import framework.SmsSenderHandler;
import framework.Text2Speech;
import framework.VibrateHandler;
import framework.WifiHandler;
import com.example.voicecommand.MainActivity;

public class VoiceEngine {
	private ComponentName mComponentName;
	private static Text2Speech speak;
	private static FlashLightHandler flash;
	private static BackLightHandler backlight;
	private static AudioProfile audpro;
	private static VibrateHandler vib;
	private static WifiHandler wf;
	private static BluetoothHandler blue;
	private Context context;
	private Activity activity;
	private SmsSenderHandler sms;
	private GetbatteryInfo bat;
	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
	public VoiceEngine(Context context, Activity activity){
		this.context = context;
		mComponentName = new ComponentName(context, MyAdminReceiver.class);
		bat = new GetbatteryInfo(context);
		speak = new Text2Speech(context);
		flash = new FlashLightHandler(context);
		backlight = new BackLightHandler(context, activity);
		audpro = new AudioProfile(context);
		vib = new VibrateHandler(context);
		wf = new WifiHandler(context);
		this.activity = activity;
		blue = new BluetoothHandler(activity);
		sms = new SmsSenderHandler(context);
		Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
		intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mComponentName);
		intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,"description");
		activity.startActivity(intent);
	}
	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
	public boolean gotword(String word){
		word = word.toLowerCase();
//			if(Regex.Contains(word, "light on")){flash.setOff();}//flashlight off
			if(Regex.Contains(word, "close")){speak.speech("Closing this apps");MyIntent.GotoHome(activity);return true;}
			else if(Regex.Contains(word, "flashlight on")){flash.setOn();speak.speech("flashlight is on");return true;}//flashlight on
			else if(Regex.Contains(word, "time")){speak.speech("the time is "+CalendarHandler.getHour()+"and"+CalendarHandler.getMinute()+"minutes");return true;}
			else if(Regex.Contains(word, "fime")){speak.speech("the time is "+CalendarHandler.getHour()+"and"+CalendarHandler.getMinute()+"minutes");return true;}
			else if(Regex.Contains(word, "day")){speak.speech("to day is "+CalendarHandler.getDaystr()+" "+CalendarHandler.getDate());return true;}
			else if(Regex.Contains(word, "month")){speak.speech("now is "+CalendarHandler.getMonthstr());return true;}
			else if(Regex.Contains(word, "lowes")){speak.speech("Closing this apps");MyIntent.GotoHome(activity);return true;}
			else if(Regex.Contains(word, "close")){speak.speech("Closing this apps");MyIntent.GotoHome(activity);return true;}
			else if(Regex.Contains(word, "night on")){flash.setOn();speak.speech("flashlight is on"); return true;}//flashlight on
			else if(Regex.Contains(word, "lastnight on")){flash.setOn();speak.speech("flashlight is on");return true;}//flashlight on
			else if(Regex.Contains(word, "lastnight off")){flash.setOff();speak.speech("flashlight is off");return true;}//flashlight off
			else if(Regex.Contains(word, "light on")){flash.setOn();speak.speech("flashlight is on");return true;}//flashlight on
			else if(Regex.Contains(word, "light off")){flash.setOff();speak.speech("flashlight is off");return true;}//flashlight off
			else if(Regex.Contains(word, "flashlight off")){flash.setOff();speak.speech("flashlight is off");return true;}//flashlight off
			else if(Regex.Contains(word, "flashlight of")){flash.setOff();speak.speech("flashlight is off");return true;}//flashlight off
			else if(Regex.Contains(word, "night off")){flash.setOff();speak.speech("flashlight is off");return true;}//flashlight off
			else if(Regex.Contains(word, "flashlight")){flash.toggle();speak.speech("toggle flashlight");return true;}//flashlight off
			else if(Regex.Contains(word, "brightness up")){backlight.adjustUp();backlight.set();speak.speech("brightness is up");return true;}//backlight adjustUp
			else if(Regex.Contains(word, "lightness up")){backlight.adjustUp();backlight.set();speak.speech("brightness is up");return true;}//backlight adjustUp
			else if(Regex.Contains(word, "brightness down")){backlight.adjustDown();backlight.set();speak.speech("brightness is down");return true;}//backlight adjustdown
			else if(Regex.Contains(word, "lightness down")){backlight.adjustDown();backlight.set();speak.speech("brightness is down");return true;}//backlight adjustdown
			else if(Regex.Contains(word, "brightness")){backlight.toggle();backlight.set();speak.speech("toggle brightness");return true;}//backlight adjustUp
			else if(Regex.Contains(word, "silent")){audpro.silent();speak.speech("silent set");return true;}//silent
			else if(Regex.Contains(word, "normal")){audpro.normal();speak.speech("normal set");return true;}//normal
			else if(Regex.Contains(word, "vibrate")){audpro.vibrate();vib.Vibrate();speak.speech("vibrate set");return true;}//vibrate
			else if(Regex.Contains(word, "wifi on")){wf.on();speak.speech("wifi is on");return true;}
			else if(Regex.Contains(word, "wifi off")){wf.off();speak.speech("wifi is off");return true;}
			else if(Regex.Contains(word, "battery level")){speak.speech("battery is"+GetbatteryInfo.Getlevel());return true;}
			else if(Regex.Contains(word, "battery")){speak.speech("battery is"+GetbatteryInfo.Getlevel());return true;}
			else if(Regex.Contains(word, "temparature")){speak.speech("temparature is"+GetbatteryInfo.Gettemperature()+"celsius");return true;}
			else if(Regex.Contains(word, "bluetooth on")){speak.speech("bluetooth is on ");blue.on(); return true;}
			else if(Regex.Contains(word, "bluetooth off")){speak.speech("bluetooth is off ");blue.off(); return true;}
			else if(Regex.Contains(word, "bluetooth of")){speak.speech("bluetooth is off ");blue.off(); return true;}
			else if(Regex.Contains(word, "bluetooth")){speak.speech("toggle bluetooth");blue.toggle(); return true;}
			else if(Regex.Contains(word, ".com")){speak.speech("Redirect to "+word);MyIntent.Visitlink(activity, word);return true;}
			else if(Regex.Contains(word, ".net")){speak.speech("Redirect to "+word);MyIntent.Visitlink(activity, word);return true;}
			else if(Regex.Contains(word, ".org")){speak.speech("Redirect to "+word);MyIntent.Visitlink(activity, word);return true;}
			else if(Regex.Contains(word, "what")){speak.speech("searching on google");MyIntent.Visitlink(activity,"google.com/m?q="+ word);return true;}
			else if(Regex.Contains(word, "call me")){speak.speech("yes "+Regex.Replace(word, "call me", " "));Variable.user=Regex.Replace(word, "call me", " ");return true;}
			else if(Regex.Contains(word, "reply")){if(Variable.ifmessage){ speak.speech("replying on text"+Regex.Replace(word, "reply", " "));sms.send(Regex.Replace(word, "reply"," "), Variable.number);return true;}}
			else if(Regex.Contains(word, "read")){speak.speech(Variable.message);return true;}
			else if(Regex.Contains(word, "reed")){speak.speech(Variable.message);return true;}
//			else if(Regex.Contains(word, "call")){
//				speak.speech(Variable.message);return true;
//				MyIntent.StartActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:0923456789")));
//
////				Intent intent = new Intent(Intent.ACTION_CALL);
////				intent.setData(Uri.parse("tel:0997621334"));
////				StartActivity(intent);
//			}
			else if(Regex.Contains(word, "dial")){
				speak.speech("Go to Dial");
				MyIntent.goToDial(activity);
				return true;
			}

			else if(Regex.Contains(word, "free")){speak.speech(Variable.message);return true;}
			else if(Regex.Contains(word, "message")){speak.speech(Variable.message);return true;}
			else if(Regex.Contains(word, "8")){speak.speech(Variable.message);return true;}
			else if(Regex.Contains(word, "listen")){speak.speech("yes "+Variable.user);FloatingMic.start();return true;}
			else if(Regex.Contains(word, "ignore")){speak.speech("yes "+Variable.user);FloatingMic.stop();return true;}
			else{
				for(int i=0;i<Variable.apps.length;i++){
					if(Regex.Contains(word, Variable.apps[i][0])){
						speak.speech("opening "+Variable.apps[i][0]);
						MyIntent.OpenApps(activity,Variable.apps[i][1]);
						Needs.Log( Variable.apps[i][0]);
						return true;
					}
				}
				Needs.Toast(context,word);
				speak.speech("No command execute");return true;
				}
		return false;
	}
	public void onBeginningOfSpeech(){
		Needs.Log("detect");
	}
	public void onEndOfSpeech() {
		//vib.Vibrate();
	}
	public void onError() {
			speak.speech("can't understand, please speak again");
	}
	public void onReadyForSpeech() {
		Needs.Toast(context,"speak");
	}
	public void onRmsChanged() {
		// when listening
	}
	//static method
	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
	public static void flashOn(){
		flash.setOn();
	}
	public static void speech(String msg){
		speak.speech( msg);
	}
	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
	public static void flashOff(){
		flash.setOff();
	}
	public static void backlightUp(){
		backlight.adjustUp();
		backlight.set();
	}
	public static void backlightDown(){
		backlight.adjustDown();
		backlight.set();
	}
	public static void WifiOn(){
		wf.on();
	}
	public static void WifiOff(){
		wf.off();
	}
	public static void SilentOn(){
		audpro.silent();
		vib.Vibrate();
	}
	public static void SilentOff(){
		audpro.normal();
	}
	public static void BluetoothOn(){
		blue.on();
	}
	public static void BluetoothOff(){
		blue.off();
	}

	public static void battery(){
		speak.speech("battery is"+GetbatteryInfo.Getlevel()+"temparature is"+GetbatteryInfo.Gettemperature()+"celsius");
	}
}
