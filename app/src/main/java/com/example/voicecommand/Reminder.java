package com.example.voicecommand;

import java.util.Calendar;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import receiver.AlarmReceiver;
import android.R.integer;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import data.Reminderdb;
import data.Variable;
import framework.CalendarHandler;
import framework.MyIntent;
import framework.Needs;

public class Reminder extends Activity {
	private Reminderdb Sql;
	EditText txt,hour,minute,date,month,year;
    ListView listView1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reminder);
		listView1 = (ListView) findViewById(R.id.m_lv_packages);
		Sql = new Reminderdb(getApplicationContext());
		txt = (EditText)findViewById(R.id.edreminder);
		hour = (EditText)findViewById(R.id.edhour);
		minute = (EditText)findViewById(R.id.edminute);
		date = (EditText)findViewById(R.id.eddate);
		month = (EditText)findViewById(R.id.edmonth);
		year = (EditText)findViewById(R.id.edyear);
		setlist();
		listView1.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Sql.deleteAllData();
				setlist();
			}
		});
	}
	public void save(View v){
		Sql.insertData("1",txt.getText().toString());
		if(hour.getText().toString().matches("\\d+")&&minute.getText().toString().matches("\\d+")&&date.getText().toString().matches("\\d+")&&month.getText().toString().matches("\\d+")){
			CalendarHandler.setHour(Integer.parseInt(hour.getText().toString()));
			CalendarHandler.setMinute(Integer.parseInt(minute.getText().toString()));
			CalendarHandler.setDate(Integer.parseInt(date.getText().toString()));
			CalendarHandler.setMonth(Integer.parseInt(month.getText().toString()));
			MyIntent.Alarm(this,AlarmReceiver.class,CalendarHandler.GetCalendar());
			Variable.reminder = txt.getText().toString();
			txt.setText("");
			setlist();
		}else{
			Needs.Toast(this,"please input number only");
		}
	}
	public void setlist(){
		  ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                  android.R.layout.simple_list_item_1, Sql.getAllList());
      listView1.setAdapter(adapter);
      hour.setText(CalendarHandler.getHour()+"");
      minute.setText(CalendarHandler.getMinute()+"");
      date.setText(CalendarHandler.getDate()+"");
      month.setText(CalendarHandler.getMonth()+"");
      year.setText(CalendarHandler.getYear()+"");
	}
	
}
