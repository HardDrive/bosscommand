package com.example.voicecommand;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import android.support.v4.app.ActivityCompat;
import android.widget.Toast;
import android.widget.ToggleButton;


import com.karumi.dexter.Dexter;
import com.karumi.dexter.DexterBuilder;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.single.PermissionListener;

import data.Appdata;
import data.Variable;
import framework.Dialogs;
import framework.MyIntent;
import framework.VoiceRecognitionListener;

public class MainActivity extends AppCompatActivity {
	public static String TAG  = MainActivity.class.getSimpleName();
	private static final int OVERLAY_PERMISSION_REQ_CODE = 100;

	private static final int REQUEST_CALL = 1;

	private static Appdata Sql;
	private Dialogs dialog;
	private static VoiceRecognitionListener vr;
	private  Toolbar myToolbar;
	private  TextView toolbar_title;

	private Button btnExit;
	private TextView userAgreement;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Sql = new Appdata(getApplicationContext());
		 getapp();
		vr = new VoiceRecognitionListener(getApplicationContext(), this);
		dialog = new Dialogs(this);

		ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},60);

		btnExit = findViewById(R.id.btnExit);
		userAgreement = findViewById(R.id.userAgreement);

		btnExit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
				builder.setTitle(R.string.app_name);
				builder.setIcon(R.mipmap.ic_launcher);
				builder.setMessage("Do you want to exit?")
						.setCancelable(false)
						.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								finish();
							}
						})
						.setNegativeButton("No", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
				AlertDialog alert = builder.create();
				alert.show();
			}
		});

		userAgreement.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.message("User Agreement","Important notice:\n" +
						"·\tBy downloading the App or clicking on the \"Accept\" button below you agree to the terms of the licence which will bind you. The terms of the licence include, in particular, limitations on liability in condition.\n" +
						"·\tIf you do not agree to the terms of this licence, we will not license the App and Documents to you and you must stop the downloading or streaming process (as applicable) now [by clicking on the \"Cancel\" button below]. In this case the downloading or streaming process will terminate.\n");
			}
		});

	}


	@RequiresApi(api = Build.VERSION_CODES.M)
	public void btnactive(View view) {
	    boolean on = ((ToggleButton) view).isChecked();

	    if (on) {
	    	MyIntent.StopService(this, FloatingMic.class);
	    } else {
			DexterBuilder.Permission builder = Dexter.withActivity(this);
			builder.withPermission(Manifest.permission.RECORD_AUDIO).withListener(new PermissionListener() {
				@Override
				public void onPermissionGranted(PermissionGrantedResponse response) {

					Toast.makeText(MainActivity.this, "onPermissionGranted", Toast.LENGTH_SHORT).show();
				}

				@Override
				public void onPermissionDenied(PermissionDeniedResponse response) {
					Toast.makeText(MainActivity.this, "onPermissionDenied", Toast.LENGTH_SHORT).show();
				}

				@Override
				public void onPermissionRationaleShouldBeShown(com.karumi.dexter.listener.PermissionRequest permission, PermissionToken token) {

				}


			}).check();
			if ((Build.VERSION.SDK_INT >= 23)) {
				if (!Settings.canDrawOverlays(this)) {
					Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
					startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
				} else if (Settings.canDrawOverlays(this)) {
					startService(new Intent(this, FloatingMic.class));
				}
			}
			else
			{
				startService(new Intent(this, FloatingMic.class));
			}
	    }
	}
	public void btnsettings(View v){
		MyIntent.StartActivity(this, ChoiceApp.class);
	}
	public void btnabout(View v){
        dialog.message("Developers","John Rey Franco");
//		dialog.message("Developers","John Rey Franco,\nPamela Ormila,\nElmie Salinas\nmarch 2015");
	}
	public void btnhelp(View v){

		dialog.message("All Command", "- Call me:name\n - Dial\n - Silent\n - Normal\n - Vibrate\n - Flashlight on\n - Flashlight off\n - Time\n - Day\n - Month\n - Battery level\n - Close");
//		dialog.message("Help","All Command\n* brightness up/down\n* flashlight on/off\n* silent/normal/vibrate\n* wifi on/off\n* " +
//				"battery level\n* temparature\n* name of website\n* if got text receive you can say read message or reply then your message\n* " +
//				"you can say what and your keyword you want to search\n* can open and close the apps . (Ex.open facebook/facebook close)\n* "
//				+ "Today/time\n* Volume up/ down\n* music\n-Play\n-Next\n-previous\n* -Stop\n Press the mic icon before stating a command");
	}
	public void btnmanualcontrol(View v){
		MyIntent.StartActivity(this, ManualControl.class);
	}
	public void btnfacedetect(View v){
		MyIntent.StartActivity(this,AndroidCamera.class);
	}
	public static void voiceon() {

		Variable.Iflisten = false;
		vr.listen();
		Log.e(TAG,"voiceon");
	}
	public static void insertapp(String name,String pack){
		Sql.open();
		Sql.insertData(name,pack);
		Sql.close();
	}
	public static void getapp(){
		Variable.apps = new String[Sql.getcount()][2];
		Sql.open();
		Cursor C = Sql.SelectAll();
		int i = 0;
 		if(C.moveToFirst()){
 			do {
 				Variable.apps[i][0]=C.getString(0);
 				Variable.apps[i][1]=C.getString(1);
 				i++;
 			}while(C.moveToNext());
 		}
 		Sql.close();
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == OVERLAY_PERMISSION_REQ_CODE) {
			startService(new Intent(this, FloatingMic.class));

		}
	}
}
