package com.example.voicecommand;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import data.Appdata;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import framework.MyIntent;
import framework.Needs;

public class ChoiceApp extends Activity {
	private Appdata Sql;
	public static final String EXTRA_PACKAGE_NAME = "package_name";
	private ListView mListView;
	private PackageListAdapter mListAdapter;
	private EditText mEtFilter;
	private ProgressDialog mLoadingDialog;
	private String mFilterText;
	private List<AppInfoCache> mAppList, mFullAppList;
	private PackageManager pm;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.app_list);
		setTitle("Select an app...");
		Sql = new Appdata(getApplicationContext());
		 mListView = (ListView)findViewById(R.id.m_lv_packages);
	        mEtFilter = (EditText)findViewById(R.id.m_et_search);
        
        
        pm = getPackageManager();
	    mLoadingDialog = new ProgressDialog(this);
	    mLoadingDialog.setIndeterminate(true);
	    mLoadingDialog.setMessage("Loading App List...");
	    mLoadingDialog.setCancelable(false);
	    mLoadingDialog.show();
	
	    Thread t = new Thread(mLoadAppLoadAndSortAppList);
	    t.start();
	}
    public void doListFilter() {
		mFilterText = mEtFilter.getText().toString().toLowerCase();
		mAppList.clear();
		if (mFilterText.contentEquals("")) {
			for (AppInfoCache appInfo : mFullAppList) {
				mAppList.add(appInfo);
			}
		} else {
			for (AppInfoCache appInfo : mFullAppList) {
				if (appInfo.getAppName().toLowerCase().contains(mFilterText)) {
					mAppList.add(appInfo);
				}
			}
		}
		mListAdapter.notifyDataSetChanged();
    }
    
    public AdapterView.OnItemClickListener mListItemClickAdapter = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			input(mAppList.get(position).getPackageName());
		}
    	
    };	
    public void input(final String Pname) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		LinearLayout mydialog = new LinearLayout(this);
		mydialog.setOrientation(1);
		final EditText input = new EditText(this);
		builder.setTitle("Command App");
		mydialog.addView(input);
		input.setHint("Enter your Command for this app");
		builder.setView(mydialog);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Sql.open();
				Sql.insertData(input.getText().toString(),Pname);
				Sql.close();
				MainActivity.getapp();
			}
		});
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		builder.show();
	}
	public void btnseearch(View v) {
		doListFilter();
	}
	public void btnview(View v) {
		MyIntent.StartActivity(this, ListviewAct.class);
	}
    private void initAssignListenersAndAdapter() {
    	mEtFilter.setText(mFilterText);
	    mListView.setAdapter(mListAdapter);
	    mListView.setOnItemClickListener(mListItemClickAdapter);
    }

    
    private Runnable mLoadAppLoadAndSortAppList = new Runnable() {

		@Override
		public void run() {
	        mAppList = new ArrayList<AppInfoCache>();
	        AppInfoCache tmpCache;
	        List<ApplicationInfo> installedApps = pm.getInstalledApplications(0);
	        for (ApplicationInfo appInfo : installedApps) {
       			tmpCache = new AppInfoCache(appInfo.loadLabel(pm).toString(), appInfo.packageName, appInfo.className);
       			mAppList.add(tmpCache);
	        }
	        Collections.sort(mAppList, new AlphaComparator());
	        mFullAppList = new ArrayList<AppInfoCache>();
	        int i = 0;
	        for (AppInfoCache appInfo : mAppList) {
	        	appInfo.setPosition(i);
	        	mFullAppList.add(appInfo);
	        	i+=1;
	        }
	        mListAdapter = new PackageListAdapter(getBaseContext());
	        mHandler.post(mFinishLoadAndSortTask);
		}
    	
    };
    
    private Runnable mFinishLoadAndSortTask = new Runnable() {

		@Override
		public void run() {
			initAssignListenersAndAdapter();
	        mLoadingDialog.dismiss();
		}
    	
    };
    
    private final Handler mHandler = new Handler();
    
    public class PackageListAdapter extends ArrayAdapter<AppInfoCache> {
    	Context c;

		public PackageListAdapter(Context context) {
			super(context, R.layout.app_list_item, mAppList);
			this.c = context;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final LayoutInflater inflater = LayoutInflater.from(c);
			View v = inflater.inflate(R.layout.app_list_item , parent, false);
			
			
			ImageView iv_icon = (ImageView) v.findViewById(R.id.pi_iv_icon);
			TextView tv_name = (TextView) v.findViewById(R.id.pi_tv_name);
			AppInfoCache ai = getItem(position);
			iv_icon.setImageDrawable(ai.getIcon());
			tv_name.setText(ai.getAppName());
			return v;
		}
		
    	
    }

    class AlphaComparator implements Comparator<AppInfoCache> {
        private final Collator   sCollator = Collator.getInstance();

        public final int compare(AppInfoCache a, AppInfoCache b) {
            String ainfo = a.getAppName();
            String binfo = b.getAppName();
            return sCollator.compare(ainfo, binfo);
        }
    }
    
    class AppInfoCache {
    	private String app_name, package_name, class_name;
    	private int position;
    	
    	public AppInfoCache(String aName, String pName, String cName) {
    		app_name = aName;
    		package_name = pName;
    		class_name = cName;
    		position = -1;
    	}
    	
    	public Drawable getIcon() {
    		try {
				return pm.getApplicationIcon(package_name);
			} catch (NameNotFoundException e) {
				return null;
			}
    	}
    	public int getPosition() {
    		return position;
    	}
    	public void setPosition (int pos) {
    		position = pos;
    	};
    	public String getAppName() {
    		return app_name;
    	}
    	
    	public String getPackageName() {
    		return package_name;
    	}
    	public String getClassName() {
    		return class_name;
    	}
    	
    }
}
