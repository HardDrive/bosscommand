package com.example.voicecommand;

import receiver.ScreenReceiver;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.ImageView;
import data.Variable;
import floating.FloatingService;
import framework.MicrophoneHandler;
import framework.MyIntent;
import framework.Needs;
import framework.ThreadHandler;

public class FloatingMic extends FloatingService {
	private View view;
	private ImageView image;
	private static ThreadHandler handler;
	private static MicrophoneHandler mic;
	private static int miclevel;
	private BroadcastReceiver ScreenReceiver;

	  
	@Override
	protected View getFloatingView() {
		this.view = LayoutInflater.from(this).inflate(R.layout.floatmic, null);
		this.init();
		return this.view;
	}

	private void init() {
		handler = new ThreadHandler(500);
		image = (ImageView) view.findViewById(R.id.downImageView);
		this.view.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				if (!isMoving()) {
					MyIntent.StartNewActivity(FloatingMic.this,
							MainActivity.class);
				}
				return false;
			}
		});

	}

	public void click(View v) {
		 MainActivity.voiceon();
	}

	public static void start() {
		mic = new MicrophoneHandler();
		handler.start();
	}

	public static void stop() {
		handler.stop();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	public static void runningtheread() {
		miclevel = mic.getLevel();
		if (miclevel < 2) {
			Variable.Notnoise = true;
		}
		if (Variable.Notnoise) {
			if (miclevel >= 10) {
				if (Variable.ifmessage) {
					VoiceEngine.speech(Variable.message);
					Variable.ifmessage = false;
					Variable.message = "no message";
				}  else {
					Needs.Log("two"+miclevel);
					if (Variable.Iflisten) {
						MainActivity.voiceon();
					}
				}
			}
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		 IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON); 
		   filter.addAction(Intent.ACTION_SCREEN_OFF);
		   ScreenReceiver = new ScreenReceiver();
		   registerReceiver(ScreenReceiver, filter);
		   MyIntent.StartNewActivity(this,MainActivity.class);
		init();
		return super.onStartCommand(intent, flags, startId);
	}
}
