package com.example.voicecommand;

import framework.MyIntent;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ToggleButton;

public class ManualControl extends AppCompatActivity{


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.manual_control);

		ImageButton btnback = findViewById(R.id.btnback);

		Button btnDial = findViewById(R.id.btnDial);

		btnback.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		btnDial.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Uri number = Uri.parse("tel:");
				Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
				startActivity(callIntent);
			}
		});
	}
	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
	public void flashlight(View view) {
	    boolean on = ((ToggleButton) view).isChecked();
	    if (on) {
	    	VoiceEngine.flashOn();
	    } else {
	    	VoiceEngine.flashOff();
	    }
	}
	public void reminder(View v){
		MyIntent.StartActivity(this,Reminder.class);
	}

	public void backlight(View view) {
	    boolean on = ((ToggleButton) view).isChecked();
	    if (on) {
	    	VoiceEngine.backlightUp();
	    } else {
	    	VoiceEngine.backlightDown();
	    }
	}
	public void wifi(View view) {
	    boolean on = ((ToggleButton) view).isChecked();
	    if (on) {
	    	VoiceEngine.WifiOn();
	    } else {
	    	VoiceEngine.WifiOff();
	    }
	}
	public void silent(View view) {
	    boolean on = ((ToggleButton) view).isChecked();
	    if (on) {
	    	VoiceEngine.SilentOn();
	    } else {
	    	VoiceEngine.SilentOff();
	    }
	}
	public void bluetooth(View view) {
	    boolean on = ((ToggleButton) view).isChecked();
	    if (on) {
	    	VoiceEngine.BluetoothOn();
	    } else {
	    	VoiceEngine.BluetoothOff();
	    }
	}
	public void battery(View v){
		VoiceEngine.battery();
	}
	public void openapp(View v){
		Intent i = new Intent(ManualControl.this,ChoiceApp.class);
		startActivity(i);
	}


}
